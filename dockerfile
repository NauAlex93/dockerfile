FROM debian:9-slim as build

ENV VER_NGINX=1.6.0
ENV VER_LUAJIT=2.0.4
ENV VER_NGINX_DEVEL_KIT=0.3.0
ENV NGINX_DEVEL_KIT=ngx_devel_kit-${VER_NGINX_DEVEL_KIT}
ENV VER_LUA_NGINX_MODULE=0.10.11
ENV LUA_NGINX_MODULE=lua-nginx-module-${VER_LUA_NGINX_MODULE}
ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.0

ENV NGINX_ROOT=/usr/local/nginx

RUN apt update && apt install -y git build-essential zlib1g-dev libpcre3 libpcre3-dev wget gcc make

RUN wget http://nginx.org/download/nginx-${VER_NGINX}.tar.gz  && \
    wget http://luajit.org/download/LuaJIT-${VER_LUAJIT}.tar.gz  && \
    wget --no-check-certificate https://github.com/simpl/ngx_devel_kit/archive/v${VER_NGINX_DEVEL_KIT}.tar.gz -O ${NGINX_DEVEL_KIT}.tar.gz  && \
    wget --no-check-certificate https://github.com/openresty/lua-nginx-module/archive/v${VER_LUA_NGINX_MODULE}.tar.gz -O ${LUA_NGINX_MODULE}.tar.gz && \
    tar -xzvf nginx-${VER_NGINX}.tar.gz && rm nginx-${VER_NGINX}.tar.gz && \
    tar -xzvf LuaJIT-${VER_LUAJIT}.tar.gz && rm LuaJIT-${VER_LUAJIT}.tar.gz && \
    tar -xzvf ${NGINX_DEVEL_KIT}.tar.gz && rm ${NGINX_DEVEL_KIT}.tar.gz && \
    tar -xzvf ${LUA_NGINX_MODULE}.tar.gz && rm ${LUA_NGINX_MODULE}.tar.gz && \
    cd LuaJIT-${VER_LUAJIT} && \
    make && make install && \
    cd ../nginx-${VER_NGINX} && \
    ./configure --prefix=${NGINX_ROOT} --with-ld-opt="-Wl,-rpath,${LUAJIT_LIB}" --add-module=../${NGINX_DEVEL_KIT} --add-module=../${LUA_NGINX_MODULE} && \
    make -j2 && make install

FROM debian:9-slim
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx /usr/local/nginx
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/lib /usr/lib
CMD ["./nginx", "-g", "daemon off;"]